package com.rustfisher.tutorial2020.act;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.rustfisher.baselib.AbsActivity;
import com.rustfisher.tutorial2020.R;

/**
 * 演示返回是传递参数
 */
public class ForResultResultLauncher1Act extends AbsActivity {
    private TextView mTitleTv;
    private TextView mSubTitleTv;

    private ActivityResultLauncher<Intent> mEditInfoLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG += "1";
        setContentView(R.layout.act_for_res_first);
        mTitleTv = findViewById(R.id.tv1);
        mSubTitleTv = findViewById(R.id.tv2);

        findViewById(R.id.edit_btn).setOnClickListener(v -> mEditInfoLauncher.launch(new Intent(getApplicationContext(), ForResultSecondAct.class)));

        mEditInfoLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            Log.i(TAG, "修改昵称返回： " + result);
            if (result.getResultCode() == RESULT_OK) {
                mTitleTv.setText(result.getData().getStringExtra(ForResultSecondAct.K_TITLE));
                mSubTitleTv.setText(result.getData().getStringExtra(ForResultSecondAct.K_SUB_TITLE));
            } else {
                Toast.makeText(getApplicationContext(), "未保存修改", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
